require 'rails_helper'

RSpec.describe '商品詳細ページ', type: :feature do
  let(:taxon) { create(:taxon) }
  let(:anoter_taxon) { create(:taxon) }
  let(:product) { create(:product, name: "hogehoge", price: 10, description: "foobar", taxons: [taxon]) }
  let!(:related_product) { create(:product, name: "hogebar", taxons: [taxon]) }
  let!(:another_product) { create(:product, name: "non_display", taxons: [anoter_taxon]) }
  let(:base_title) { 'BIGBAG Store' }

  describe 'showテンプレートの場合' do
    before do
      visit potepan_product_path(product.id)
    end

    it 'タイトル「（商品名）- BIGBAG Store」を表示する' do
      expect(page).to have_title "#{product.name} - #{base_title}"
    end

    it '商品名・価格・説明が表示される' do
      expect(page).to have_content "hogehoge"
      expect(page).to have_content 10
      expect(page).to have_content "foobar"
    end

    it '関連商品は表示されるが関連していない商品と商品自身は表示されない' do
      within '.productsContent' do
        expect(page).to have_content "hogebar"
        expect(page).not_to have_content "non_display"
        expect(page).not_to have_content "hogehoge"
      end
    end

    it 'トップページへのリンクをクリックすると遷移する' do
      click_link "Home", match: :prefer_exact
      expect(current_path).to eq potepan_path
    end

    it 'ロゴをクリックするとトップページへ遷移する' do
      click_on "BIGBAG"
      expect(current_path).to eq potepan_path
    end

    describe '遷移のテスト' do
      context 'カテゴリーページにいた場合' do
        before do
          visit potepan_category_path(taxon.id)
          click_on product.name
        end

        it '「一覧ページへ戻る」をクリックすると元いたカテゴリーページへ遷移する' do
          click_on "一覧ページへ戻る"
          expect(current_path).to eq potepan_category_path(taxon.id)
        end
      end

      context 'カテゴリーページ以外にいた場合' do
        let!(:root_taxon) { create(:taxon, id: 1) }

        before do
          visit potepan_path
          visit potepan_product_path(product.id)
        end

        it '「一覧ページへ戻る」をクリックするとポテパンページへは遷移しない' do
          click_on "一覧ページへ戻る"
          expect(current_path).not_to eq potepan_path
          expect(current_path).to eq potepan_category_path(root_taxon.id)
        end
      end

      context '関連商品をクリックした場合' do
        before do
          click_on "hogebar"
        end

        it 'その商品の詳細ページへ遷移する' do
          expect(current_path).to eq potepan_product_path(related_product.id)
          expect(page).to have_title "#{related_product.name} - #{base_title}"
          within '.productsContent' do
            expect(page).not_to have_content "hogebar"
          end
        end
      end
    end
  end
end
