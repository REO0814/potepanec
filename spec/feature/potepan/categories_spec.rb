require 'rails_helper'

RSpec.describe 'カテゴリーページ', type: :feature do
  let(:taxonomy1) { create(:taxonomy, name: 'Categories') }
  let!(:taxon1) { create(:taxon, name: "BAGS", taxonomy: taxonomy1, parent: taxonomy1.root) }
  let!(:taxon2) { create(:taxon, name: "MUGS", taxonomy: taxonomy1, parent: taxonomy1.root) }
  let!(:product1) { create(:product, name: "foo", price: 10, taxons: [taxon1]) }
  let!(:product2) { create(:product, name: "bar", price: 20, taxons: [taxon2]) }
  let(:base_title) { 'BIGBAG Store' }

  describe 'showテンプレートの場合' do
    before do
      visit potepan_category_path(taxon1.id)
    end

    it 'タイトル「BAGS - BIGBAG Store」が表示されている' do
      expect(page).to have_title "#{taxon1.name} - #{base_title}"
    end

    it 'トップページへのリンクをクリックすると遷移する' do
      click_link "Home", match: :prefer_exact
      expect(current_path).to eq potepan_path
    end

    it 'ロゴをクリックするとトップページへ遷移する' do
      click_on "BIGBAG"
      expect(current_path).to eq potepan_path
    end

    it '商品をクリックすると個別ページへ遷移する' do
      within ".productBox" do
        click_on product1.name
      end
      expect(current_path).to eq potepan_product_path(product1.id)
    end

    it '商品カテゴリー名・カテゴリー名・taxon1に紐付く商品名と画像が表示されている' do
      within ".side-nav" do
        expect(page).to have_content taxonomy1.name
        click_on taxonomy1.name
        expect(page).to have_content taxon1.name
        expect(page).to have_content taxon2.name
      end
      within ".productBox" do
        expect(page).to have_content product1.name
        expect(page).to have_content product1.display_price
        expect(page).not_to have_content product2.name
        expect(page).not_to have_content product2.display_price
      end
    end

    context "CATEGORIESのMUGSをクリックする" do
      before do
        within ".side-nav" do
          click_on taxon2.name
        end
      end

      it 'taxon2のパスになっている' do
        expect(current_path).to eq potepan_category_path(taxon2.id)
      end

      it 'taxon2に紐付く商品名・価格が表示される' do
        within ".productBox" do
          expect(page).not_to have_content product1.name
          expect(page).not_to have_content product1.display_price
          expect(page).to have_content product2.name
          expect(page).to have_content product2.display_price
        end
      end
    end
  end
end
