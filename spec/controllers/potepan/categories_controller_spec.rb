require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "GET #show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:products) { create(:product, taxons: [taxon]) }

    before do
      get :show, params: { id: taxon.id }
    end

    it "showへのアクセスに対して正常なレスポンスが返ってくる" do
      expect(response).to be_successful
    end

    it "showテンプレートが表示される" do
      expect(response).to render_template :show
    end

    it "@taxonomiesが期待された値を持っている" do
      expect(assigns(:taxonomies)).to contain_exactly(taxonomy)
    end

    it "@taxonが期待された値を持っている" do
      expect(assigns(:taxon)).to eq taxon
    end

    it "@productsが期待された値を持っている" do
      expect(assigns(:products)).to contain_exactly(products)
    end
  end
end
