require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "GET #show" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      get :show, params: { id: product.id }
    end

    it "showへのアクセスに対して正常なレスポンスが返ってくる" do
      expect(response).to be_successful
    end

    it "showテンプレートが表示される" do
      expect(response).to render_template :show
    end

    it "@productが期待された値を持っている" do
      expect(assigns(:product)).to eq product
    end

    it "＠related_productsの要素が４つある" do
      expect(assigns(:related_products).count).to eq 4
    end
  end
end
