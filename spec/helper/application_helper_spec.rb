require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe 'full_titleヘルパー' do
    let(:base_title) { 'BIGBAG Store' }

    context 'デフォルト値の場合' do
      let(:no_title) { full_title('') }

      it 'タイトル「BIGBAG Store」が表示される' do
        expect(no_title).to eq "#{base_title}"
      end
    end

    context '引数にnilが渡された場合' do
      let(:nil_title) { full_title(nil) }

      it 'タイトル「BIGBAG Store」が表示される' do
        expect(nil_title).to eq "#{base_title}"
      end
    end

    context '引数に「TEST」が渡された場合' do
      let(:title) { full_title('TEST') }

      it 'タイトル「TEST - BIGBAG Store」が表示される' do
        expect(title).to eq "TEST - #{base_title}"
      end
    end
  end
end
