module Potepan::TaxonomyDecorator
  def all_children
    taxons.where.not(parent_id: nil)
  end

  Spree::Taxonomy.prepend self
end
